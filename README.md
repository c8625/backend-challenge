# CORE Backend - IT Challenge


## Task

Build a system capable of receiving 100k transactions without collapsing. The system must store all the transactions in a csv. The system will have 3 interfaces:
- CLI
- Api
- FTP

### CLI

Cli must allow to check the current throughput of transactions, size of the master csv and allow to reset all the system (swipe all transactions)

### API
Api must provide a CRUD of transactions, specification must be written in Open Api 3.0 . It must handle up to 100k concurrent users adding transactions. Also must indicate the current ftp file being processed among the status of that processing, and allow the cancellation of mentioned file.

### FTP
FTP interface should upload any formatted csv transaction file that is uploaded into the ftp and process them.

**The csv writing must be handled by a service that should limit the writing requests up to 5 x second**

## Evaluation

The items evaluated in this challenge will be:
1. System architecture
2. Code architecture ( Try Clean architecture)
3. Interface design
4. Documentation (interfaces documentation, code documentation, decisions documentation, architecture documentation (UML in images))
5. Testability (unit testing, tdd, code coverage, pentesting, stress testing… whatever you use (or don't) will be evaluated)
6. Git project structure, usage  and management (we will check your commits, dev process should be shown there)
7. Dependencies needed
8. Deployment (easy to deploy, production ready)
9. Functionality (must work)

All items must be correctly explained and justified (if needed) in the README.md file

## Allowed technologies

- Javascript
- Typescript
- Git
- SQL Databases
- Document based Databases
- npm packages (as long as there is a justification for using it)
- Rabbit MQ
- Docker
- Anything else that you consider needed and you can justify

## Allowed decisions
You can design whatever you want as long as the requirements are fulfilled. Transactions can be whatever you want. Also you can deliver faked external services (for instance the FTP server) as long as the faking is good enough.

## How to deliver
Senda mail to challenge@koibanx.com with the link to the repo with the solution

#Deadlien
June 15th 2022

